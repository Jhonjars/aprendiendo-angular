// export class DestinoViaje{
//     nombre:string;
//     imagenUrl:string;

//     constructor(n:string, u:string){
//         this.nombre = n;
//         this.imagenUrl = u;
//     }
// }
// la afirmacion superior es equivalente a la siguiente sin el selected y los metodos


import {v4 as uuid} from 'uuid';

export class DestinoViaje{
  selected: boolean;
  servicios: string[];
  id = uuid();

  constructor( public nombre:string, public imagenUrl:string, public votes: number=0){
    this.servicios= ['pileta', 'desayuno'];
  }
    isSelected(): boolean{
      return this.selected;
    }
    setSelected(s: boolean){
      this.selected= s;
    }
    voteUp(): any {
      this.votes++;
    }
    voteDown(): any {
      this.votes--;
    }
}

